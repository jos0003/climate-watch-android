package org.earthwatch.climatewatch;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class TabsPagerAdapter extends FragmentStatePagerAdapter {

	UserProfile userProfile;
	Sighting sighting;
	BrowsePosts browsePosts;

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);

		userProfile = new UserProfile();
		sighting = new Sighting();
		browsePosts = new BrowsePosts();

	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {

		case 0:
			return userProfile;
		case 1:
			return sighting;
		case 2:
			return browsePosts;// new Social();
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

}
