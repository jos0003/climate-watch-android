package org.earthwatch.climatewatch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class NetworkHelper {
	
	public static int UploadImage(String sourceFilePath, String sourceFileName, int pid) {
        
		String upLoadServerUri = "http://crazychimps.com/climatewatch/upload_image.php";

	    int serverResponseCode = 0;
        
        String fileName = sourceFilePath + sourceFileName;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;  
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024; 
        File sourceFile = new File(fileName); 
         
        if (!sourceFile.isFile()) {
             return 0;
        }
        else
        {
             try { 
                  
            	 Bitmap bmp = BitmapFactory.decodeFile(fileName);
    			 ByteArrayOutputStream bos = new ByteArrayOutputStream();
    			 bmp.compress(CompressFormat.JPEG, 70, bos);
    			 InputStream fileInputStream = new ByteArrayInputStream(bos.toByteArray());
                   // open a URL connection to the Servlet
                 //InputStream fileInputStream = new FileInputStream(sourceFile);
                 URL url = new URL(upLoadServerUri);
                  
                 // Open a HTTP  connection to  the URL
                 conn = (HttpURLConnection) url.openConnection(); 
                 conn.setDoInput(true); // Allow Inputs
                 conn.setDoOutput(true); // Allow Outputs
                 conn.setUseCaches(false); // Don't use a Cached Copy
                 conn.setRequestMethod("POST");
                 conn.setRequestProperty("Connection", "Keep-Alive");
                 conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                 conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                 conn.setRequestProperty("uploaded_file", fileName); 
                  
                 dos = new DataOutputStream(conn.getOutputStream());
        
                 dos.writeBytes(twoHyphens + boundary + lineEnd); 
                 dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                                           + fileName + "\"" + lineEnd);
                  
                 dos.writeBytes(lineEnd);
        
                 // create a buffer of  maximum size
                 bytesAvailable = fileInputStream.available(); 
        
                 bufferSize = Math.min(bytesAvailable, maxBufferSize);
                 buffer = new byte[bufferSize];
        
                 // read file and write it into form...
                 bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
                    
                 while (bytesRead > 0) {
                      
                   dos.write(buffer, 0, bufferSize);
                   bytesAvailable = fileInputStream.available();
                   bufferSize = Math.min(bytesAvailable, maxBufferSize);
                   bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
                    
                  }
        
                 // send multipart form data necesssary after file data...
                 dos.writeBytes(lineEnd);
                 dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        
                 // Responses from the server (code and message)
                 serverResponseCode = conn.getResponseCode();
                 String serverResponseMessage = conn.getResponseMessage();
                   
                 Log.i("uploadFile", "HTTP Response is : "
                         + serverResponseMessage + ": " + serverResponseCode);
                  
                 if(serverResponseCode == 200){
                	 // successfully uploaded
                 }    
                  
                 //close the streams //
                 fileInputStream.close();
                 dos.flush();
                 dos.close();
                 conn.disconnect();
                 
                 // Tell the server what the pid of that image was:
     		    List<NameValuePair> params = new ArrayList<NameValuePair>();
 			    params.add(new BasicNameValuePair("pid", Integer.toString(pid)));
 			    params.add(new BasicNameValuePair("name", sourceFileName));
 			    String url_image_id = "http://crazychimps.com/climatewatch/upload_image_path.php";
				JSONParser jsonParser = new JSONParser();
				JSONObject json = jsonParser.makeHttpRequest(url_image_id, "POST",
					params);
                   
            } catch (MalformedURLException ex) {
                 
                ex.printStackTrace();
                 
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);  
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Upload file to server Exception", "Exception : "
                                                 + e.getMessage(), e);  
            }
            return serverResponseCode; 
             
         } // End else block 
	} 
	
	public static int UploadProfileImage(String sourceFilePath, String sourceFileName, String username) {
        
		String upLoadServerUri = "http://crazychimps.com/climatewatch/upload_image.php";

	    int serverResponseCode = 0;
        
        String fileName = sourceFilePath + sourceFileName;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;  
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024; 
        File sourceFile = new File(fileName); 
         
        if (!sourceFile.isFile()) {
             return 0;
        }
        else
        {
             try { 
                  
            	 Bitmap bmp = BitmapFactory.decodeFile(fileName);
    			 ByteArrayOutputStream bos = new ByteArrayOutputStream();
    			 bmp.compress(CompressFormat.JPEG, 70, bos);
    			 InputStream fileInputStream = new ByteArrayInputStream(bos.toByteArray());
                   // open a URL connection to the Servlet
                 //InputStream fileInputStream = new FileInputStream(sourceFile);
                 URL url = new URL(upLoadServerUri);
                  
                 // Open a HTTP  connection to  the URL
                 conn = (HttpURLConnection) url.openConnection(); 
                 conn.setDoInput(true); // Allow Inputs
                 conn.setDoOutput(true); // Allow Outputs
                 conn.setUseCaches(false); // Don't use a Cached Copy
                 conn.setRequestMethod("POST");
                 conn.setRequestProperty("Connection", "Keep-Alive");
                 conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                 conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                 conn.setRequestProperty("uploaded_file", fileName); 
                  
                 dos = new DataOutputStream(conn.getOutputStream());
        
                 dos.writeBytes(twoHyphens + boundary + lineEnd); 
                 dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                                           + fileName + "\"" + lineEnd);
                  
                 dos.writeBytes(lineEnd);
        
                 // create a buffer of  maximum size
                 bytesAvailable = fileInputStream.available(); 
        
                 bufferSize = Math.min(bytesAvailable, maxBufferSize);
                 buffer = new byte[bufferSize];
        
                 // read file and write it into form...
                 bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
                    
                 while (bytesRead > 0) {
                      
                   dos.write(buffer, 0, bufferSize);
                   bytesAvailable = fileInputStream.available();
                   bufferSize = Math.min(bytesAvailable, maxBufferSize);
                   bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
                    
                  }
        
                 // send multipart form data necesssary after file data...
                 dos.writeBytes(lineEnd);
                 dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        
                 // Responses from the server (code and message)
                 serverResponseCode = conn.getResponseCode();
                 String serverResponseMessage = conn.getResponseMessage();
                   
                 Log.i("uploadFile", "HTTP Response is : "
                         + serverResponseMessage + ": " + serverResponseCode);
                  
                 if(serverResponseCode == 200){
                	 // successfully uploaded
                 }    
                  
                 //close the streams //
                 fileInputStream.close();
                 dos.flush();
                 dos.close();
                 conn.disconnect();
                 
                 // Tell the server what the pid of that image was:
     		    List<NameValuePair> params = new ArrayList<NameValuePair>();
 			    params.add(new BasicNameValuePair("username", username));
 			    params.add(new BasicNameValuePair("name", sourceFileName));
 			    String url_image_id = "http://crazychimps.com/climatewatch/upload_profile_image_path.php";
				JSONParser jsonParser = new JSONParser();
				JSONObject json = jsonParser.makeHttpRequest(url_image_id, "POST",
					params);
                   
            } catch (MalformedURLException ex) {
                 
                ex.printStackTrace();
                 
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);  
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Upload file to server Exception", "Exception : "
                                                 + e.getMessage(), e);  
            }
            return serverResponseCode; 
             
         } // End else block 
	} 
	
	public int Vote(int pid, int upvote, int downvote){
		new Vote().execute(pid,upvote, downvote);
		return 0;
	}
	
	class Vote extends AsyncTask<Integer, Integer, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		}

		/**
		 * Creating product
		 * */
		protected String doInBackground(Integer... args) {

			String url_vote = "http://crazychimps.com/climatewatch/vote.php";
			int pid = args[0];
			int upvote = args[1];
			int downvote = args[2];
			
			System.out.println(args);
		    // Building Parameters
		    List<NameValuePair> params = new ArrayList<NameValuePair>();
		    params.add(new BasicNameValuePair("pid", Integer.toString(pid)));
		    params.add(new BasicNameValuePair("upvote", Integer.toString(upvote)));
		    params.add(new BasicNameValuePair("downvote", Integer.toString(downvote)));

			System.out.println(params);

		    // getting JSON Object
		    // Note that create product URL accepts POST method
			JSONParser jsonParser = new JSONParser();
		    JSONObject json = jsonParser.makeHttpRequest(url_vote, "GET",
			    params);

		    return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		}
	}
	
}
