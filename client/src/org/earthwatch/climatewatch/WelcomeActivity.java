package org.earthwatch.climatewatch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class WelcomeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_welcome);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.welcome, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_about) {
	    Intent intent = new Intent(this, About.class);
	    startActivity(intent);
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    public void gotoLoginScreen(View view) {
	Intent intent = new Intent(this, LoginActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
	startActivity(intent);
    overridePendingTransition(0,0);
    }

    public void gotoSignUpScreen(View view) {
	Intent intent = new Intent(this, SignUpActivity.class);
	startActivity(intent);
    }
}
