<?php
// Any error, warning, information or other messages should be defined here.

// Reason for using strings.php: Localization (as an extensible feature)
// BUT: https://www.youtube.com/watch?v=0j74jcxSunY

$strings_en = array();

$strings_en['no_post'] = "No post found.";
$strings_en['req_fields_missing'] = "Required field(s) is missing.";
$strings_en['no_prod'] = "No products.";
$strings_en['post_success'] = "Post successfully created.";
$strings_en['general_err'] = "Oops! An error occurred.";

// Use English localization
$strings = $strings_en;
?>
