<?php
	 
	/*
	 * Following code will list all the posts
	 */
	 
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	 
	// connecting to db
	$db = new DB_CONNECT();
	
	if(isset($_GET['pid'])){
		
		$pid = mysql_real_escape_string($_GET['pid']);
		 
		$result = mysql_query("SELECT * FROM comments WHERE pid = '$pid'") or die(mysql_error());
		 
		$response = array();
		 
		if (mysql_num_rows($result) > 0) {
			$response["comments"] = array();
			while ($row = mysql_fetch_array($result)) {
				// temp user array
				$post= array();
				$name = $row["name"];
				$comment = $row["comment"];
				$post["name"] = $name;
				$post["comment"] = $comment;
				
				$account = mysql_query("SELECT * FROM accounts WHERE username = '$name'");
				$prof_image_url = mysql_fetch_array($account)['profile_image_url'];
				
				$post["profile_image_url"] = $prof_image_url;
				
				// push single posts into final response array
				array_push($response["comments"], $post);
			}
			// success
			$response["success"] = 1;
			
			// echoing JSON response
			echo json_encode($response);
		} else {
			$response["success"] = 0;
			$response["message"] = "No comments found";
			echo json_encode($response);
		}
	}
?>