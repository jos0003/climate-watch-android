<?php
		
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	
	// connecting to db
	$db = new DB_CONNECT();
	
	// array for JSON response
	$response = array();
	
	$file_path = "images/";
	 
	// check for required fields
	if (isset($_POST['username']) && isset($_POST['name'])) {
	 
		$username= mysql_real_escape_string($_POST['username']);
		$name = mysql_real_escape_string($_POST['name']);
		
		$path = $file_path . $name;
		
		// mysql inserting a new row
		$result = mysql_query("UPDATE accounts SET profile_image_url='$path' WHERE username = '$username'");
 
	 	if($result){
			$response["success"] = 1;
			$response["message"] = "post successfully created.";
		 
		        // echoing JSON response
		        echo json_encode($response);
		} else {
		        // failed to insert row
		        $response["success"] = 0;
		        $response["message"] = "Oops! An error occurred.";
		 
		        // echoing JSON response
		        echo json_encode($response);
	        }
	        
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
		
		// echoing JSON response
		echo json_encode($response);
	}
?>