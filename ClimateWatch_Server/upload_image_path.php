<?php
	 
	/*
	 * Following code will create a new product row
	 * All image details are read from HTTP Post Request
	 */
		
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	
	// connecting to db
	$db = new DB_CONNECT();
	 
	// array for JSON response
	$response = array();
	
	$file_path = "images/";
	 
	// check for required fields
	if (isset($_POST['pid']) && isset($_POST['name'])) {
	 
		$pid= mysql_real_escape_string($_POST['pid']);
		$name = mysql_real_escape_string($_POST['name']);
		
		$path = $file_path . $name;
		
		// mysql inserting a new row
		 $result = mysql_query("INSERT INTO images(pid, path) VALUES('$pid', '$path')");
 
	 	if($result){
			$response["success"] = 1;
			$response["message"] = "post successfully created.";
		 
		        // echoing JSON response
		        echo json_encode($response);
		} else {
		        // failed to insert row
		        $response["success"] = 0;
		        $response["message"] = "Oops! An error occurred.";
		 
		        // echoing JSON response
		        echo json_encode($response);
	        }
	        
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
		
		// echoing JSON response
		echo json_encode($response);
	}
?>