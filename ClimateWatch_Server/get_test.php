<?php
	 
	/*
	 * Following code will list all the products
	 */
	 
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	 
	// connecting to db
	$db = new DB_CONNECT();
	 
	$result = mysql_query("SELECT *FROM posts") or die(mysql_error());
	 
	$response = array();
	 
	if (mysql_num_rows($result) > 0) {
		$response["posts"] = array();
		while ($row = mysql_fetch_array($result)) {
			// temp user array
			$post= array();
			$post["pid"] = $row["pid"];
			$post["name"] = $row["name"];
			$post["title"] = $row["title"];
			
			// push single product into final response array
			array_push($response["posts"], $post);
		}
		// success
		$response["success"] = 1;
		
		// echoing JSON response
		echo json_encode($response);
	} else {
		$response["success"] = 0;
		$response["message"] = "No products found";
		echo json_encode($response);
	}
?>