<?php
	 
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	
	// connecting to db
	$db = new DB_CONNECT();
	 
	// array for JSON response
	$response = array();
	 
	// check for required fields
	if (isset($_GET['pid']) && isset($_GET['upvote']) && isset($_GET['downvote'])) {
	 
		$pid = mysql_real_escape_string($_GET['pid']);
		
		$result = array();
		
		$record= mysql_query("SELECT * FROM posts WHERE pid = '$pid'");
		$upvote_value = mysql_fetch_array($record)['upvotes'];
		$new_up_value = $upvote_value + $_GET['upvote'];
		$result = mysql_query("UPDATE posts SET upvotes='$new_up_value' WHERE pid = '$pid'");
		$downvote_value = mysql_fetch_array($record)['downvotes'];
		$new_down_value = $downvote_value  + $_GET['downvote'];
		$result = mysql_query("UPDATE posts SET downvotes='$new_down_value ' WHERE pid = '$pid'");
 
	 	if($result){
			$response["success"] = 1;
			$response["message"] = "Vote was cast successfully.";
		 
		        // echoing JSON response
		        echo json_encode($response);
		} else {
		        // failed to insert row
		        $response["success"] = 0;
		        $response["message"] = "Oops! An error occurred.";
		 
		        // echoing JSON response
		        echo json_encode($response);
	        }
	        
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
		
		// echoing JSON response
		echo json_encode($response);
	}
?>